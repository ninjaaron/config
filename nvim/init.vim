:set number
:set relativenumber
:set autoindent
:set tabstop=4
:set shiftwidth=4
:set smarttab
:set softtabstop=4
:set mouse=a
:set expandtab

" Source files
source $HOME/.config/nvim/vim-plug/plugins.vim
source $HOME/.config/nvim/plug-config/coc.vim


" color scheme
set t_Co=256
set background=dark
let g:solarized_termcolors=16
let g:solarized_termtrans=1
colorscheme solarized

" NerdTree customizations
map <F3> :NERDTreeToggle<CR>

